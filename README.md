This is a vulnerable APK built from [InjuredAndroid](https://github.com/B3nac/InjuredAndroid). We use this APK for testing MobSF which is maintained by SAST team.
